# OpenML dataset: Santander_transaction_value

https://www.openml.org/d/42572

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

According to Epsilon research, 80% of customers are more likely to do business with you if you provide personalized service. Banking is no exception.

The digitalization of everyday lives means that customers expect services to be delivered in a personalized and timely manner... and often before they've even realized they need the service. In their 3rd Kaggle competition, Santander Group aims to go a step beyond recognizing that there is a need to provide a customer a financial service and intends to determine the amount or value of the customer's transaction. This means anticipating customer needs in a more concrete, but also simple and personal way. With so many choices for financial services, this need is greater now than ever before.

In this competition, Santander Group is asking Kagglers to help them identify the value of transactions for each potential customer. This is a first step that Santander needs to nail in order to personalize their services at scale.

You are provided with an anonymized dataset containing numeric feature variables, the numeric target column, and a string ID column.

The task is to predict the value of target column.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42572) of an [OpenML dataset](https://www.openml.org/d/42572). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42572/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42572/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42572/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

